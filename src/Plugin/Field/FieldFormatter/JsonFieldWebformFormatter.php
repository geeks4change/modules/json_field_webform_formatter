<?php

namespace Drupal\json_field_webform_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\json_field_webform_formatter\Plugin\WebformHandler\WebformTwigFormatterHandler;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the JSON Field Webform formatter.
 *
 * @FieldFormatter(
 *   id = "json_field_webform_formatter",
 *   label = @Translation("Webform for JSON field"),
 *   field_types = {
 *     "json",
 *     "json_native",
 *     "json_native_binary",
 *   },
 * )
 */
class JsonFieldWebformFormatter extends FormatterBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityFieldManagerInterface $entityFieldManager;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->entityTypeManager = $container->get('entity_type.manager');
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  public static function defaultSettings() {
    return [
      'webform_reference_field' => '',
      'webform_id' => '',
    ] + parent::defaultSettings();
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['webform_reference_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Webform reference'),
      '#default_value' => $this->getSetting('webform_reference_field'),
      '#options' => $this->fieldOptions($this->fieldDefinition),
      '#empty_value' => '',
    ];
    $form['webform_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Webform ID'),
      '#default_value' => $this->getSetting('webform_id'),
      '#options' => array_map(
        fn(WebformInterface $webform) => $webform->label(),
        Webform::loadMultiple()
      ),
      '#empty_value' => '',
    ];
    return $form;
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {

    $webformReferenceFieldName = $this->settings['webform_reference_field'];
    $entity = $items->getEntity();
    if (
      $entity->hasField($webformReferenceFieldName)
      && ($webformReferenceField = $entity->get($webformReferenceFieldName))
      && $webformReferenceField instanceof EntityReferenceFieldItemListInterface
    ) {
      // First try webform reference.
      $webform = $webformReferenceField->referencedEntities()[0] ?? NULL;
    }
    else {
      // Then use fallback.
      $webformId = $this->settings['webform_id'];
      $webform = Webform::load($webformId);
    }

    if (!$webform) {
      return [];
    }

    $twigHandlers = array_values(array_filter(
      iterator_to_array($webform->getHandlers()),
      fn(WebformHandlerInterface $handler) => $handler->getPluginId() === 'json_field_webform_formatter_twig'
    ));
    if ($twigHandlers) {
      $twigHandler = $twigHandlers[0];
      assert($twigHandler instanceof WebformTwigFormatterHandler);
      $twig = $twigHandler->getTwig();
      $build = $this->viewViaTwig($items, $twig);
      (new CacheableMetadata())
        ->addCacheableDependency($items->getEntity())
        // Handler is part of webform config.
        ->addCacheableDependency($webform)
        ->applyTo($build);
    }
    else {
      // Handler config is part of webform anyway, so cacheability already done.
      $build = $this->viewNative($items, $webform->id());
    }

    return $build;
  }

  public function viewViaTwig(FieldItemListInterface $items, string $twig) {
    $jsonValues = array_map(
      fn(FieldItemInterface $item) => json_decode($item->get('value')->getValue(), TRUE),
      iterator_to_array($items)
    );
    return [
      '#type' => 'inline_template',
      '#template' => $twig,
      '#context' => [
        'data' => $jsonValues,
        'entity' => $items->getEntity(),
        'field_name' => $items->getName(),
      ],
    ];
  }

  protected function viewNative(FieldItemListInterface $items, string $webformId): array {
    // JSON field usually has only one items, but this works fur multuiple too.
    $dataItems = array_map(
      fn(FieldItemInterface $item) => json_decode($item->get('value')->getValue(), TRUE),
      iterator_to_array($items)
    );
    // Create one or multiple webform submissions out of this.
    try {
      $webformSubmissions = array_map(
        fn(array $data) => WebformSubmission::create(['webform_id' => $webformId])
          ->setData($data),
        $dataItems
    );
    } catch (EntityStorageException $e) {
      return [];
    }

    $webformSubmissionViewBuilder = $this->entityTypeManager->getHandler('webform_submission', 'view_builder');
    assert($webformSubmissionViewBuilder instanceof EntityViewBuilderInterface);

    $build = $webformSubmissionViewBuilder->viewMultiple($webformSubmissions);
    // The resulting build is simply #theme=[entityId],...
    // @see \Drupal\webform\WebformSubmissionViewBuilder::getBuildDefaults
    foreach (Element::children($build) as $child) {
      // @see json_field_webform_formatter_webform_submission_view_alter()
      $build[$child]['#hide_empty'] = TRUE;
    }
    return $build;
  }

  private function fieldOptions(FieldDefinitionInterface $fieldDefinition): array {
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($fieldDefinition->getTargetEntityTypeId(), $fieldDefinition->getTargetBundle());
    $webformReferenceFieldDefinitions = array_filter($fieldDefinitions,
      fn(FieldDefinitionInterface $fieldDefinition) => $fieldDefinition->getSetting('target_type') === 'webform'
    );
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $options = array_map(
      fn(FieldDefinitionInterface $fieldDefinition) => $fieldDefinition->getLabel(),
      $webformReferenceFieldDefinitions
    );
    return $options;
  }

}
