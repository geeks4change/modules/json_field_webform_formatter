<?php

namespace Drupal\json_field_webform_formatter\Plugin\WebformHandler;

use Drupal\codemirror_editor\Element\CodeMirror;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform\Plugin\WebformHandlerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform formatter twig handler.
 *
 * @WebformHandler(
 *   id = "json_field_webform_formatter_twig",
 *   label = @Translation("Webform Twig formatter"),
 *   category = @Translation("Geeks4Change"),
 *   description = @Translation("Use twig to format webform data."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
final class WebformTwigFormatterHandler extends WebformHandlerBase {

  protected TwigEnvironment $twigEnvironment;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->twigEnvironment = $container->get('twig');
    return $instance;
  }

  public function defaultConfiguration() {
    return parent::defaultConfiguration()
      + ['twig' => ''];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['twig'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Twig'),
      // Consider hijacking the complete webform submission data (!) display.
      '#description  ' => $this->t('Only used in JSON field webform formatter.'),
      '#required' => TRUE,
      '#default_value' => $this->getTwig(),
    ];
    if (class_exists(CodeMirror::class)) {
      $form['twig']['#type'] = 'codemirror';
      $form['twig']['#codemirror'] = [
        'mode' => 'text/x-twig',
        'lineNumbers' => TRUE,
      ];
    }
    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    // Sorry, no easy way to validate twig here.
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  public function getTwig() {
    return $this->getSetting('twig');
  }

}
